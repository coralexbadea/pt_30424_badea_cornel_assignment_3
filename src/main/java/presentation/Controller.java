package main.java.presentation;

import main.java.bll.ClientBLL;
import main.java.bll.OrderBLL;
import main.java.bll.ProductBLL;
import main.java.bll.SupplierBLL;
import main.java.start.Start;

import java.util.List;
import java.util.logging.Logger;

/**
 * Controller class - A class used to bind the user interface with the actual business-database model
 * It calls specific methods and instantiate Objects from the BLL package for executing the command received
 * The results are then transferred to the methods from the View Class
 */
public class Controller {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
    private List<String> commands;
    private ClientBLL clientBLL;
    private OrderBLL orderBLL;
    private ProductBLL productBLL;
    private SupplierBLL supplierBLL;
    private View view;
    public Controller(List<String> commands){
        this.commands = commands;
        this.clientBLL = new ClientBLL();
        this.orderBLL = new OrderBLL();
        this.productBLL = new ProductBLL();
        this.supplierBLL = new SupplierBLL();
        this.view  = new View();
    }

    /**
     * executeCommands method - method used for executing commands in the form of strings
     * @throws Exception - Exception is thrown if a command can not be executed for various reasons
     */
    public void executeCommands() throws Exception {
        for (String command : commands) {
            System.out.println(command);
            String[] splitCommand = command.split(" ");
            switch (splitCommand[0]) {//get the first part of the command
                case "Insert": //Insert command
                    handleInsert(splitCommand[1], command);
                    break;
                case "Delete": //Delete command
                    handleDelete(splitCommand[1], command);
                    break;
                case "Order:": //Order command
                        int toPay = orderBLL.insertOrder(command);
                        if(toPay > 0)
                            this.view.printBill(splitCommand[1], toPay);
                        else
                            this.view.printUnderStock(splitCommand[1]);
                    break;
                case "Report": //Report command
                    handleReports(splitCommand[1]);
                    break;

                default:
                    System.out.println(splitCommand[0]);//Something Else
                    throw (new Exception("Wrong line format in input file for line:" + command));
            }
        }
    }

    /**
     * handleInsert method - method called for handling a insert command
     * @param type - A string denoting the subtype of command
     * @param command - the actual command to be handled
     * @throws Exception - Exception during the execution of the command
     */
    private void handleInsert(String type, String command) throws Exception {
        if(type.equals("client:")){//client command subtype
            clientBLL.insertClient(command);
        }else if(type.equals("product:")){//product command subtype
            productBLL.insertProduct(command);
        }else if(type.equals("supplier:")){//supplier command subtype
            supplierBLL.insertSupplier(command);
        }
    }
    /**
     * handleDelete method - method called for handling a delete command
     * @param type - A string denoting the subtype of command
     * @param command - the actual command to be handled
     * @throws Exception - Exception during the execution of the command
     */
    private void handleDelete(String type, String command) throws Exception {
        if(type.equals("client:")){// client command subtype
            clientBLL.deleteClient(command);
        }else if(type.equals("product:")){//product command subtype
            productBLL.deleteProduct(command);
        }
        else if(type.equals("order:")){//order command subtype
            orderBLL.deleteOrder(command);
        }
        if(type.equals("supplier:")){//supplier command subtype
            supplierBLL.deleteSupplier(command);
        }
    }
    /**
     * handleDelete method - method called for handling a Report command
     * @param type - A string denoting the subtype of command
     */
    private void handleReports(String type) {
        switch (type) {
            case "clients":
                List < ?>clients = clientBLL.report();
                this.view.printList(clients, "clients");
                break;
            case "products":
                List < ?>products = productBLL.report();
                this.view.printList(products, "products");
                break;
            case "orders":
                List < ?>orders = orderBLL.report();
                this.view.printList(orders, "orders");
                break;
            case "suppliers":
                List < ?>suppliers = supplierBLL.report();
                this.view.printList(suppliers, "suppliers");
                break;
        }
    }


}
