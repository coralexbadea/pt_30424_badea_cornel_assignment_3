package main.java.presentation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * View class - Class used to handle the UI of the application
 */
public class View {

    private int billCount; // variable for tracking the numbers of bills
    private int reportCount; // variable for tracking the numbers of normal reports
    private int stockCount; // variable for tracking the numbers of out of stock reports

    public View(){
        billCount = 0;
        reportCount = 0;
        stockCount = 0;
    }

    /**
     * printList method - used for printing a list of Objects in PDF format
     * @param list - the generic List to pe printed
     * @param type - the type of the Objects in the list // used only for title aesthetics
     */
    public void printList(List<?> list, String  type){
        List<String> toPrint;
        Document document = new Document();

        try
        {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Report" +type + reportCount+ ".pdf"));
            reportCount += 1;
            document.open();
            for(Object obj: list) {
                toPrint = ReflectionExample.retrieveProperties(obj);
                for(String string : toPrint)
                    document.add(new Paragraph(string));
                document.add(new Paragraph(" "));
            }
            document.close();
            writer.close();
        } catch (DocumentException e)
        {
            e.printStackTrace();
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }


    }

    /**
     * printBill method - Used for printing a Bill in PDF format
     * @param name - The name of the charged
     * @param toPay - the amount to be paid
     */
    public void printBill(String name, int toPay){

        Document document = new Document();
        try
        {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Bill" + billCount + ".pdf"));
            billCount += 1;
            document.open();
            document.add(new Paragraph("Dear "+name+" your order has been successful"));
            document.add(new Paragraph("Price paid:" + toPay));
            document.close();
            writer.close();
        } catch (DocumentException e)
        {
            e.printStackTrace();
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * printUnderStack method - Used for printing an under-stock message in PDF formmat
     * @param name - the name of the client requiring the unavailable operation
     */
    public void printUnderStock(String name){

        Document document = new Document();
        try
        {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Out_Of_Stock" + stockCount + ".pdf"));
            stockCount += 1;
            document.open();
            document.add(new Paragraph("Dear "+name+" your order could not be processed"));
            document.add(new Paragraph("The product selected is not in stock anymore"));
            document.add(new Paragraph("Please check later"));
            document.close();
            writer.close();
        } catch (DocumentException e)
        {
            e.printStackTrace();
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }

}
