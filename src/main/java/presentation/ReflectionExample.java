package main.java.presentation;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for handling some reflection functionalities
 */
public class ReflectionExample {

	/**
	 * retrieveProprieties method - method to retrieve the fields and values for an Instantiated Object
	 * @param object - the Object from where to retrieve the desired values
	 * @return - a list of Strings representing the retrieved fields and values
	 */
	public static List<String> retrieveProperties(Object object) {
		List<String> result = new ArrayList<>();
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true); 
			Object value;
			try {
				value = field.get(object);
				result.add(field.getName() + "=" + value);

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}
		return result;
	}
}



