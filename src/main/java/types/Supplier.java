package main.java.types;

/**
 * Supplier class - Mapping a row in the database Supplier table
 */
public class Supplier {
    String name;
    String email;

    public Supplier(){
        name = "";
        email = "";
    }
    public Supplier(String name){
        this.name = name;
        this.email = "";
    }
    public Supplier(String name, String email){
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
