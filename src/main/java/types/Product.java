package main.java.types;

/**
 * Product class - Mapping a row in the database Product table
 */
public class Product {
    String name;
    int quantity;
    int price;
    String name_supplier;

    public Product(){};
    public Product(String name){
        this.name = name;
        this.quantity = 0;
        this.price = 0;
        this.name_supplier = "null";
    }
    public Product(String name, int quantity, int price){
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.name_supplier = "null";
    }
    public Product(String name, int quantity, int price, String name_supplier){
        this(name, quantity, price);
        this.name_supplier = name_supplier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName_supplier() {
        return name_supplier;
    }

    public void setName_supplier(String name_supplier) {
        this.name_supplier = name_supplier;
    }
}
