package main.java.types;

/**
 * Client class - Mapping a row in the database Client table
 */
public class Client {

    private String name;
    private String city;

    public Client(){
        name = "";
        city = "";
    };
    public Client(String name){
        this.name = name;
        city = "";
    }
    public Client(String name, String city){
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
