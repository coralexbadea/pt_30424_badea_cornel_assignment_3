package main.java.types;

/**
 * Order class - Mapping a row in the database Order table
 */
public class Order {
    String name_client;
    String name_product;
    int quantity;

    public Order(){}
    public Order(String name_client) {
        this.name_client = name_client;
        this.name_product = "";
        this.quantity = 0;
    }
    public Order(String name_client, String name_product) {
        this.name_client = name_client;
        this.name_product = name_product;
        this.quantity = 0;
    }
    public Order(String name_client, String name_product, int quantity) {
        this.name_client = name_client;
        this.name_product = name_product;
        this.quantity = quantity;
    }

    public String getName_client() {
        return name_client;
    }

    public void setName_client(String name_client) {
        this.name_client = name_client;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
