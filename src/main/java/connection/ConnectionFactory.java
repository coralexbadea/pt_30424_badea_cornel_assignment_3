package main.java.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConnectionFactory {

	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());//the Logger Object used to print warning messages
	private static final String DRIVER = "com.mysql.jdbc.Driver";// the driver used to connect to the database
	private static final String DBURL = "jdbc:mysql://127.0.0.1:3306/assignmentdb?useSSL=false";// the jdbc URL used to connect to the database
	private static final String USER = "root";//username for database
	private static final String PASS = "root";//password for database
	//Using the singleton pattern
	private static ConnectionFactory singleInstance = new ConnectionFactory();

	private ConnectionFactory() {
		try {

			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * createConnection method - used to create the connection with the database
	 * @return The Connection object
	 */
	private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(DBURL, USER, PASS);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
			e.printStackTrace();
		}
		return connection;
	}

	/**
	 * getConnection method - used to get access to the connection provided by the singleton Object
	 * @return  the Connection Object
	 */
	public static Connection getConnection() {
		return singleInstance.createConnection();
	}

	/**
	 * close method - Used to close the connection
	 * @param connection - Connection Object representing the connection with the database
	 */
	public static void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
			}
		}
	}

	/**
	 * close method - used to close a statement
	 * @param statement - The statement Object representing the statement to be closed
	 */
	public static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
			}
		}
	}

	/**
	 * close method - used to close a result-set
	 * @param resultSet - The ResultSet Object representing the result-set to be closed
	 */
	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
			}
		}
	}
}
