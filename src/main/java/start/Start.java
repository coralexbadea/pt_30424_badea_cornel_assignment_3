package main.java.start;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;


import main.java.presentation.Controller;


public class Start {

	/**
	 * checkArgs - method used to check the number of arguments passed to the program
	 * @param args - the arguments received in the command line
	 */
	private static void checkArgs(String[] args){
		if(args.length != 1){
			System.out.println("Usage:java -jar PT2020_30424_Cornel_Badea_Assignment_3.jar <input file> ");
			System.exit(1);
		}

	}

	/**
	 * readFile method - Used for reading form a file
	 * @param fileName - the name of the input file
	 * @return - A list of fetched lines from the file
	 */
	private static List<String> readFile(String fileName)
	{

		List<String> records = new ArrayList<>();
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = reader.readLine()) != null)
			{
				records.add(line);
			}
			reader.close();
			if(records.size() == 0){
				System.err.format("Wrong file format in file: '%s'.", fileName);
				System.exit(2);
			}
			return records;
		}
		catch (Exception e)
		{
			System.err.format("Exception occurred trying to read '%s'.", fileName);
			e.printStackTrace();
			System.exit(3);
			return null;
		}
	}

	/**
	 * The main function
	 * @param args - commandline arguments
	 */
	public static void main(String[] args){
		checkArgs(args);
		List<String> commands = readFile(args[0]);
		Controller controller = new Controller(commands);
		try{
			controller.executeCommands();
		}catch(Exception e){
			System.err.println("Error occurred while executing commands");
			System.err.println(e.getMessage());
		}
	}

}
