package main.java.dao;

import main.java.connection.ConnectionFactory;
import main.java.types.Order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
/**
 * OrderDAO class - class that extends the AbstractDAO instantiating it's generic type
 * It contains only Order specific methods
 */
public class OrderDAO extends AbstractDAO<Order> {
    /**
     * findByClient method - Used to find rows in the Order table that has a specific value
     * in the name_client field
     * @param name - String representing the name of the client that determines the fetched rows from table Orders
     * @return - The Objects representing the retrieved rows, in a List
     */
    public List<Order> findByClient(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("name_client");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,   "OrderDAO:findByClient " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }
    /**
     * findByProduct method - Used to find rows in the Order table that has a specific value
     * in the name_product field
     * @param name - String representing the name o the product that determines the fetched rows from table Orders
     * @return - The Objects representing the retrieved rows, in a List
     */
    public List<Order> findByProduct(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("name_product");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,   "OrderDAO:findByProduct " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }
}
