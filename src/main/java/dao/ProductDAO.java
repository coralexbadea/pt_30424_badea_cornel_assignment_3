package main.java.dao;

import main.java.connection.ConnectionFactory;
import main.java.types.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

/**
 * ProductDAO class - class that extends the AbstractDAO instantiating it's generic type
 * It contains only Product specific methods
 */
public class ProductDAO extends AbstractDAO<Product> {

    /**
     * findBySupplier method - Used to find rows in the Product table that has a specific value
     * in the name_supplier field
     * @param supplier - String representing the name of th supplier that determines the fetched rows from table Product
     * @return - The Objects representing the retrieved rows, in a List
     */
    public List<Product> findBySupplier(String supplier) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("name_supplier");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, supplier);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,   "ProductDAO:findBySupplier " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

}
