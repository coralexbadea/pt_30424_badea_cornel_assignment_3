package main.java.dao;

import main.java.types.Client;

/**
 * ClientDAO class - class that extends the AbstractDAO instantiating it's generic type
 * It contains only Client specific methods
 */
public class ClientDAO extends AbstractDAO<Client> {

	// uses basic CRUD methods from superclass

	// TODO: create only student specific queries
}
