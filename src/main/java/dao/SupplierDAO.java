package main.java.dao;

import main.java.types.Supplier;

/**
 * SupplierDAO class - class that extends the AbstractDAO instantiating it's generic type
 * It contains only supplier specific methods
 */
public class SupplierDAO extends AbstractDAO<Supplier> {
}
