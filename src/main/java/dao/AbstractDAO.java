package main.java.dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.sql.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.connection.ConnectionFactory;

/**
 * AbstractDAO class - Generic type class used for database access
 * @param <T> - generic type that will denote the actual table that we access and Objects
 *           that we work with in this class
 */
public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		//the Class Object denoting the Instance of the generic type - found at runtime
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	/**
	 * createSelectQuery method - used to create a query type String  for selecting the row with a specific field
	 * @param field - the field that denotes the  row type selected by the generated query
	 * @return - a query String
	 */
	public String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM `");
		sb.append(type.getSimpleName());
		sb.append("` WHERE " + field + " =?");
		return sb.toString();
	}

	/**
	 * createFindAllQuery method - used for creating a query to select all the rows in a specific table
	 * @return - the query in the form of a String
	 */
	private String createFindAllQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append("`"+type.getSimpleName()+"`");
		
		return sb.toString();
	}

	/**
	 * findAll method - method used to retrieve all the rows of a table as a list of Object
	 * @return -the List of Objects representing the rows in the table
	 */
	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createFindAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return (List<T>) createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * findByName method - Used to fetch a specific row of a table with a specific name value
	 * @param name - the name value that denote the searched row
	 * @return -  the Object representing the fetched row
	 */
	public T findByName(String name) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("name");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1, name);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * createObjects method - Used for creating a list of Objects based on a result-set Object
	 * @param resultSet - the ResultSet Object from where to derive the specific objects
	 * @return - a list of Objects
	 */
	public List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * createInsertSQL method - used to create an SQL statement for inserting an object
	 * @param t - generic type T Object that will be determined at runtime
	 * @return -  a string representing the actual SQL statement
	 */
	private String createInsertSQL(T t){
		StringBuilder string = new StringBuilder();
		string.append("INSERT INTO `" + t.getClass().getSimpleName() + "` (");
		for(Field field : t.getClass().getDeclaredFields()){
			string.append("`" + field.getName()+"`,");
		}

		string.deleteCharAt(string.length()-1);
		string.append(") VALUES (");
		for (Field field : t.getClass().getDeclaredFields()) {

			field.setAccessible(true);
			Object value;
			try {
				value = field.get(t);
				string.append("'"+ value + "', ");
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}

		string.deleteCharAt(string.length()-2);
		string.append(")");
		String result = string.toString();
		System.out.println(result);
		return result;

	}

	/**
	 * createDeleteSQL method - used to create a SQL statement for deleting a row with
	 * value o the attributes determined by the parameter Object
	 * @param t - generic type T Object that will be determined at runtime
	 * @return -  a string representing the actual SQL statement
	 */
	private String createDeleteSQL(T t) {

		StringBuilder string = new StringBuilder();
		string.append("DELETE FROM `" + t.getClass().getSimpleName() + "` WHERE ");
		for(Field field : t.getClass().getDeclaredFields()){
			field.setAccessible(true);
			Object value;
			try {
				value = field.get(t);
				if(value instanceof String && value.equals("")) continue;
					if(value instanceof Integer && (Integer) value == 0) continue;
					string.append(field.getName()+"='"+value+"' AND ");
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		string.delete(string.length()-4, string.length()-1);

		String result = string.toString();
		System.out.println(result);
		return result;

	}

	/**
	 * createUpdateSQL method - used to create a SQL statement for updating a row with
	 * value o the attributes determined by the parameter Object
	 * @param t - generic type T Object that will be determined at runtime
	 * @return -  a string representing the actual SQL statement
	 */
	private String createUpdateSQL(T t){
		StringBuilder string = new StringBuilder();
		string.append("UPDATE " + t.getClass().getSimpleName() + " SET ");
		Field[] fields = t.getClass().getDeclaredFields();
		for(int  i = 1; i < fields.length; i++){
			fields[i].setAccessible(true);
			Object value;
			try {
				value = fields[i].get(t);
				string.append(fields[i].getName()+"='"+value+"' , ");
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		string.delete(string.length()-3, string.length()-1);
		string.append("WHERE ");
		Field fieldPK = t.getClass().getDeclaredFields()[0];
		fieldPK.setAccessible(true);
		try {
			Object value = fieldPK.get(t);
			string.append(fieldPK.getName()+"='"+value+"'");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		String result = string.toString();
		System.out.println(result);
		return result;
	}

	/**
	 * insert method - Used to insert a row with values of the attributes determined
	 * by the Object parameter
	 * @param t - generic type T Object that will be determined at runtime
	 */
	public void insert(T t) {
		Connection connection = null;
		Statement statement = null;
		String sql = createInsertSQL(t);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}

		System.out.println("Inserting");
	}
	/**
	 * insert method - Used to delete a row with values of the attributes determined
	 * by the Object parameter
	 * @param t - generic type T Object that will be determined at runtime
	 */
	public void delete(T t) {
		Connection connection = null;
		Statement statement = null;
		String sql = createDeleteSQL(t);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}

		System.out.println("Deleting");
	}
	/**
	 * insert method - Used to delete a row with values of the attributes determined
	 * by the Object parameter
	 * @param t - generic type T Object that will be determined at runtime
	 */
	public void update(T t) {
		Connection connection = null;
		Statement statement = null;

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			String sql = createUpdateSQL(t);
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}

		System.out.println("Updating");


	}
}
