package main.java.bll;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import main.java.bll.validators.ClientExistsValidator;
import main.java.bll.validators.Validator;
import main.java.dao.AbstractDAO;
import main.java.dao.ClientDAO;
import main.java.dao.OrderDAO;

import main.java.types.Client;
import main.java.types.Order;

/**
 * ClientBLL - This class is responsible with the business logic for the Client type
 * */
public class ClientBLL {
    private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    Validator<Order> clientValidator;
    public ClientBLL(){
        clientValidator = new ClientExistsValidator();
    }
    /**
     * insertClient method - This method is used to insert a client with the attributes that will be extracted from the command
     * @param command - This is the raw command that is read from the input file
     * @throws Exception - The exception is due to the invalid number of attributes that may occur
     */
    public void insertClient(String command) throws Exception {
        String[] firstSplit = command.split(":");
        String[] attributes = firstSplit[1].split(",");
        if(attributes.length != 2) {
            throw (new Exception("Invalind number of attributes in :" + command));
        }else
        {
           Client client = new Client(attributes[0].substring(1), attributes[1].substring(1));
           new ClientDAO().insert(client);
        }

    }

    /**
     * deleteClient method - This method is used to delete the client with the attributes that will be extracted from the command argument
     * @param command - This is the raw command that is read from the input file
     * @throws Exception - The exception is due to the invalid number of attributes that may occur
     */
    public void deleteClient(String command) throws Exception {
        String[] firstSplit = command.split(":");
        String[] attributes = firstSplit[1].split(",");
        if(attributes.length > 2) {
            throw (new Exception("Invalind number of attributes in :" + command));
        }else
        {
            Client client = new Client();

            switch (attributes.length) {
                case 2:
                    client = new Client(attributes[0].substring(1), attributes[1].substring(1));// a client with two attributes may be deleted
                    break;
                case 1:
                    client = new Client(attributes[0].substring(1));//a client with one attribute may be deleted - more generalized
                    break;
            }

            List<Order> orders = new OrderDAO().findByClient(client.getName());
            for(Order order: orders){
                new OrderDAO().delete(order);
            }
            try {
                clientValidator.validate(new Order(client.getName(),"", 0));
                new ClientDAO().delete(client); // delete the actual client
            }catch (Exception e) {//print the warning with LOGGER
                LOGGER.log(Level.WARNING, "ProductBLL:deleteProduct" + e.getMessage());
            }

        }

    }

    /**
     * report method - This method is used to call the DAO method findAll for the table of Clients
     * @return - The list of Clients
     */
    public List<?> report(){
        return new ClientDAO().findAll();
    }

    /**
     * findByName method - this method is used to call the DAO method findByName
     * @param name - the name to search for in the Client table
     * @return - The row fetched form the table
     */
    public Client findByName(String name) {
        return new ClientDAO().findByName(name);
    }
}
