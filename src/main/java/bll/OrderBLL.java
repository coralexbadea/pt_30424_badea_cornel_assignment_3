package main.java.bll;


import main.java.bll.validators.*;
import main.java.dao.AbstractDAO;
import main.java.dao.OrderDAO;
import main.java.dao.ProductDAO;

import main.java.types.Order;

import main.java.types.Product;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * OrderBLL - This class is responsible with the business logic for the Order type
 * */
public class OrderBLL {
    private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    private Validator<Order> clientValidator;
    private Validator<Order> productValidator;
    public OrderBLL() {
        clientValidator = new ClientExistsValidator();
        productValidator = new ProductExistsValidator();

    }
    /**
     * insertOrder method - This method is used to insert an order with the attributes that will be extracted from the command
     * @param command - This is the raw command that is read from the input file
     * @throws Exception - The exception is due to the invalid number of attributes that may occur
     * @return - the price to be paid after the placing of the order
     */
    public int insertOrder(String command) throws Exception {
        String[] firstSplit = command.split(":");
        String[] attributes = firstSplit[1].split(",");
        if(attributes.length != 3) {
            throw (new Exception("Invalind number of attributes in :" + command));
        }else
        {
            Order order = new Order(attributes[0].substring(1), attributes[1].substring(1), Integer.parseInt(attributes[2].substring(1)));
            clientValidator.validate(order);
            productValidator.validate(order);
            Product product = new ProductDAO().findByName(order.getName_product());
            if(product.getQuantity() >= order.getQuantity()) {

                try {
                    clientValidator.validate(order);
                    productValidator.validate(order);
                    new OrderDAO().insert(order);
                    product.setQuantity(product.getQuantity() - order.getQuantity());
                    new ProductDAO().update(product);
                    return (product.getPrice() * order.getQuantity());
                } catch (Exception e) {//print the warning with LOGGER
                    LOGGER.log(Level.WARNING, "SupplierBLL:insertSupplier " + e.getMessage());
                }

            }
            return 0;
        }

    }
    /**
     * deleteOrder method - This method is used to delete the order with the attributes that will be extracted from the command argument
     * @param command - This is the raw command that is read from the input file
     * @throws Exception - The exception is due to the invalid number of attributes that may occur
     */
    public void deleteOrder(String command) throws Exception {
        String[] firstSplit = command.split(":");
        String[] attributes = firstSplit[1].split(",");
        if(attributes.length > 3) {
            throw (new Exception("Invalind number of attributes in :" + command));
        }else
        {
            Order order = new Order();

            switch (attributes.length) {
                case 3:
                    order = new Order(attributes[0].substring(1), attributes[1].substring(1), Integer.parseInt(attributes[2].substring(1))); //can delete order based on 3 attributes
                    break;
                case 2:
                    order = new Order(attributes[0].substring(1), attributes[1].substring(1));// can delete order based on 2 attributes
                    break;
                case 1:
                    order = new Order(attributes[0].substring(1)); //can delete order based on 1 attribute
                    break;
            }

            new OrderDAO().delete(order);
        }

    }



    /**
     * report method - This method is used to call the DAO method findAll for the table of Orders
     * @return - The list of Orders
     */
    public List<?> report(){
        List<Order> reports = new OrderDAO().findAll();
        return reports;
    }
}
