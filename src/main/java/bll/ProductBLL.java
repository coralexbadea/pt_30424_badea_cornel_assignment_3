package main.java.bll;


import main.java.bll.validators.ProductExistsValidator;
import main.java.bll.validators.SupplierExistsValidator;
import main.java.bll.validators.Validator;
import main.java.dao.*;
import main.java.types.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ProductBLL - This class is responsible with the business logic for the Product type
 * */
public class ProductBLL {
    private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    //validator for the Product type
    private Validator<Product> supplierValidator;
    private Validator<Order> productValidator;
    public ProductBLL() {

        //validator to validate if the supplier exists
        supplierValidator = new SupplierExistsValidator();
        productValidator = new ProductExistsValidator();
    }

    /**
     * insertProduct method - This method is used to insert a product with the attributes that will be extracted from the command
     * @param command - This is the raw command that is read from the input file
     * @throws Exception - The exception is due to the invalid number of attributes that may occur
     */
    public void insertProduct(String command) throws Exception {
        String[] firstSplit = command.split(":");
        String[] attributes = firstSplit[1].split(",");
        if(attributes.length < 3 || attributes.length > 4) {
            throw (new Exception("Invalind number of attributes in :" + command));
        }else
        {

        Product product = new Product(attributes[0].substring(1), Integer.parseInt(attributes[1].substring(1)), Integer.parseInt(attributes[2].substring(1))); //we can insert a product with 3 attributes
        if(attributes.length == 4)
            product.setName_supplier(attributes[3].substring(1)); //we can integrate the supplier if it is specified;
        try{
            supplierValidator.validate(product);
            new ProductDAO().insert(product);//insert the product
        }catch (Exception e) {//print the warning with LOGGER
            LOGGER.log(Level.WARNING, "ProductBLL:insertProduct " + e.getMessage());
        }
        }

    }
    /**
     * deleteProduct method - This method is used to delete the product with the attributes that will be extracted from the command argument
     * @param command - This is the raw command that is read from the input file
     * @throws Exception - The exception is due to the invalid number of attributes that may occur
     */
    public void deleteProduct(String command) throws Exception {
        String[] firstSplit = command.split(":");
        String[] attributes = firstSplit[1].split(",");
        if(attributes.length > 4) {
            throw (new Exception("Invalind number of attributes in :" + command));
        }else
        {
            Product product = new Product();

            switch (attributes.length) {
                case 4:
                    product = new Product(attributes[0].substring(1), Integer.parseInt(attributes[1].substring(1)), Integer.parseInt(attributes[2].substring(1)), attributes[3].substring(1));
                    break;
                case 3:
                    product = new Product(attributes[0].substring(1), Integer.parseInt(attributes[1].substring(1)), Integer.parseInt(attributes[2].substring(1)));
                    break;
                case 1:
                    product = new Product(attributes[0].substring(1));
                    break;
            }
            //if the supplier is specified verify if it exists
            if(!product.getName_supplier().equals("null"))
                supplierValidator.validate(product);
            List<Order> orders = new OrderDAO().findByProduct(product.getName());//find all orders that are made for the product deleted
            for(Order order: orders){
                new OrderDAO().delete(order); // delete all orders contains the deleted product
            }
            try {
                productValidator.validate(new Order("",product.getName(), 0));
                new ProductDAO().delete(product); // delete the actual product
            }catch (Exception e) {//print the warning with LOGGER
                LOGGER.log(Level.WARNING, "ProductBLL:deleteProduct" + e.getMessage());
            }
        }

    }

    /**
     * findBySupplier method - this method is used to call the DAO method findBySupplier
     * @param supplier - the supplier to search for in the Product table
     * @return - The row fetched form the table
     */
    public List<Product> getBySupplier(String supplier){

        return new ProductDAO().findBySupplier(supplier);

    }
    /**
     * findByName method - this method is used to call the DAO method findByName
     * @param name - the name to search for in the Product table
     * @return - The row fetched form the table
     */
    public Product findByName(String name) {
        Product prd = new ProductDAO().findByName(name);
        return prd;
    }

    /**
     * report method - This method is used to call the DAO method findAll for the table of Products
     * @return - The list of Products
     */
    public List<?> report(){
        return new ProductDAO().findAll();
    }
}
