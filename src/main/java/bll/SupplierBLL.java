package main.java.bll;

import main.java.bll.validators.EmailValidator;
import main.java.bll.validators.Validator;
import main.java.dao.*;
import main.java.dao.SupplierDAO;
import main.java.types.Product;
import main.java.types.Supplier;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ProductBLL - This class is responsible with the business logic for the Product type
 * */
public class SupplierBLL {
    private Validator<Supplier> emailValidator;

    private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    public SupplierBLL() {
        //Validator for validating the email
        emailValidator = new EmailValidator();
    }
    /**
     * insertSupplier method - This method is used to insert a supplier with the attributes that will be extracted from the command
     * @param command - This is the raw command that is read from the input file
     * @throws Exception - The exception is due to the invalid number of attributes that may occur
     */
    public void insertSupplier(String command) throws Exception {
        String[] firstSplit = command.split(":");
        String[] attributes = firstSplit[1].split(",");
        if(attributes.length != 2) {
            throw (new Exception("Invalind number of attributes in :" + command));
        }else
        {
            Supplier supplier = new Supplier(attributes[0].substring(1), attributes[1].substring(1));
            try {// try to validate the email that will throw an exception in case of failing
                emailValidator.validate(supplier);
                new SupplierDAO().insert(supplier);
            }catch(Exception e){//print the warning with LOGGER
                LOGGER.log(Level.WARNING,  "SupplierBLL:insertSupplier " + e.getMessage());
            }

        }
    }


    /**
     * deleteSupplier method - This method is used to delete the supplier with the attributes that will be extracted from the command argument
     * @param command - This is the raw command that is read from the input file
     * @throws Exception - The exception is due to the invalid number of attributes that may occur
     */
    public void deleteSupplier(String command) throws Exception {
        String[] firstSplit = command.split(":");
        String[] attributes = firstSplit[1].split(",");
        if(attributes.length > 2) {
            throw (new Exception("Invalind number of attributes in :" + command));
        }else
        {
            Supplier supplier = new Supplier();

            switch (attributes.length) {
                case 2:
                    supplier = new Supplier(attributes[0].substring(1), attributes[1].substring(1));//can delete a supplier based on 2 attributes
                    break;
                case 1:
                    supplier = new Supplier(attributes[0].substring(1));//can delete a supplier based on 1 attribute
                    break;
            }
            List<Product> products = new ProductBLL().getBySupplier(supplier.getName()); //Get all the product supplied by the supplier and delete then
            for(Product product : products) {
                new ProductDAO().delete(product);
            }
            new SupplierDAO().delete(supplier);// delete the actual supplier
        }

    }

    /**
     * report method - This method is used to call the DAO method findAll for the table of Suppliers
     * @return - The list of Suppliers
     */
    public List<?> report(){
        return new SupplierDAO().findAll();
    }
    /**
     * findByName method - this method is used to call the SupplierDAO method findByName
     * @param name_supplier - the name to search for in the Supplier table
     * @return - The row fetched form the table
     */
    public Supplier findByName(String name_supplier) {
        return new SupplierDAO().findByName(name_supplier);
    }
}
