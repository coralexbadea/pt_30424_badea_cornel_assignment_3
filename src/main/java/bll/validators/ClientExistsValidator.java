package main.java.bll.validators;

import main.java.bll.ClientBLL;
import main.java.types.Client;
import main.java.types.Order;

/**
 * ClientExistsValidator - Validator Class used to check if a client exists
 */
public class ClientExistsValidator implements Validator<Order> {


    @Override
    public void validate(Order order) {
        Client foundClient = new ClientBLL().findByName(order.getName_client());
        if(foundClient == null){
            throw new IllegalArgumentException("Client's name is not a valid name!");
        }
    }
}
