package main.java.bll.validators;

import main.java.bll.SupplierBLL;
import main.java.types.Product;
import main.java.types.Supplier;
/**
 * SupplierExistsValidator - Validator Class used to check if a supplier exists
 */
public class SupplierExistsValidator implements Validator<Product> {


    @Override
    public void validate(Product product) throws Exception {
        Supplier foundSupplier = new SupplierBLL().findByName(product.getName_supplier());
        if(foundSupplier == null){
            throw new IllegalArgumentException("Supplier's name is not a valid name!");
        }
    }
}
