package main.java.bll.validators;

/**
 * Validator Interface - Interface for all validators
 * @param <T> - the generic type describing every validator at runtime
 */
public interface Validator<T> {
	//implements validate method
	public void validate(T t) throws Exception;
}
