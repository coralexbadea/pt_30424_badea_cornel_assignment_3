package main.java.bll.validators;

import main.java.bll.ProductBLL;
import main.java.types.Order;
import main.java.types.Product;

/**
 * ProductExistsValidator - Validator Class used to check if a product exists
 */
public class ProductExistsValidator implements Validator<Order> {

    @Override
    public void validate(Order order) throws Exception {
        Product foundProduct = new ProductBLL().findByName(order.getName_product());
        if(foundProduct == null){
            throw new IllegalArgumentException("Product's name is not a valid name!");
        }
    }
}
